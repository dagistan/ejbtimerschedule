package dagistan.sample.timerejb;

import java.util.Date;

import javax.ejb.Schedule;
import javax.ejb.Stateless;

@Stateless
public class ScheduleTimer {

	@Schedule(second = "*/5", minute = "*", hour = "*", persistent = false)
	public void showCurrentDate() {
		try {
			System.out.println(new Date());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
