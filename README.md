# README #

Run Glassfish. Then Deploy the project using Add/Remove of Server panel of Glassfish. Read the console values.

### Used Technologies for all projects ###

* Java SE,
* Java EE,
* 3rd party Java frameworks / libraries.

### Requirements ###

* Eclipse IDE
* JDK 1.8
* Glassfish 4.1 (for enterprise samples)
* Deployment and Run instructions are included in related project class files. Read carefully.

I hope you enjoy :)